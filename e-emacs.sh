function e () {
    # A wrapper function for emacs and emacsclient.  Handles incoming and
    # outgoing pipes, templates
    #
    ### Examples:
    ##
    ## Open $file using emacsclient and starts emacs if there's not already
    ## server running.
    #
    # e [file]
    #
    ##  Normal emacsclient flags gets passed along.  So [c]reate a new frame and
    # [n]o-wait for the client to exit.
    #
    # e -c -n file
    #
    ## Create a new buffer containing the output of a command
    #
    # ls | e
    #
    ## Create a buffer from the output of a command, edit the results, and pass
    ## it on down the pipe.
    #
    # ls -1 *.bak | e | xargs -I{} rm {}
    #
    ## Start with an empty buffer, then pass the buffer on down the pipe.
    #
    # e | rot13  # NOTE: Super Secure encryption
    #
    ## Edit a file, then pass the file on down the pipe.
    #
    # e request.txt | nc www.exampke.com 80
    #
    ## Edit a file as a template, then passing it to the pipe
    #
    # e -t header_template.txt | nc www.example.com 80
    #
    ## Edit multiple files as templates, then passing them to the pipe
    #
    # e -t header_template.html body_template.html footer.html > new_page.html
    #
    ## Keep the new templates (-k implies -t)
    #
    # e -t -k request-template.html | nc www.example.com 80
    #
    ## Store the output of editing the template in ./new/
    ## -o implies -t and -k
    #
    # e -o ./new/ template.html | nc www.example.com 80
    # NOTE: Usage on remote hosts assumes you've set up the remote
    # emacsclient to speak to the local server.
    #
    # Example:
    #
    # Set emacs server to tcp and listening on port 5922 then:
    #
    # function forward_emacs_ssh {
    #     # LC_VARs get passed with the default openssh server settings.
    #     # Assumes the hostname is the last argument
    #     LC_HOSTNAME=${*: -1} LC_EMACS=1 LC_EMACS_SERVER=$(cat ~/.emacs.d/server/server) \
    #                    ssh -R 5922:127.0.0.1:5922  -A "$@"
    # }
    #
    # And on the receiving side in .bashrc or whatever:
    #
    # if [[ "$LC_EMACS_SERVER" ]]; then
    #     ## We're in an SSH session from inside emacs, since openssh allows $LC
    #     ## variables to be passed by default.  (At least on Debian 10.4)
    #     mkdir -p ~/.emacs.d/server 2>&1 > /dev/null
    #     echo "$LC_EMACS_SERVER" > ~/.emacs.d/server/server
    # fi
    #
    #
    # NOTE: By default it uses the output of the remote hosts `hostname -f`
    # to determine the ssh_hostname.  If this is incorrect then set
    # ssh_hostname using another method, like above.
    #
    # NOTE: The --template / -t option creates temporary copies of the
    # file(s) to use.  They are deleted afterwards unless --keep / -k is
    # given.

    local EMACSOPT TMP TEMPLATES KEEP RUNID OUTPUTDIR TMPFILES=() FILES=()
    ssh_hostname=${ssh_hostname:-$(hostname -f)}
    RUNID=$(uuidgen)
    OUTPUTDIR="/tmp"
    # TODO: Add option to specify path to save templates to.

    # Read the command line arguments and separate options from files.
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -t|--template) TEMPLATES=1; shift;;

            -k|--keep) TEMPLATES=1; KEEP=1; shift;;

            -o|--output-dir) OUTPUTDIR="$2"; TEMPLATES=1; KEEP=1; shift ; shift;;

            -*) EMACSOPT+="$key "; shift;;

            *) if [[ "$TEMPLATES" ]]; then
                   local filename extension
                   filename=$(basename -- "$key")
                   extension="${filename##*.}"
                   filename="${filename%.*}"
                   TMP="${OUTPUTDIR}/${filename}-${RUNID}.${extension}"
                   cp "$key" "$TMP"
                   FILES+=("$TMP")
                   TMPFILES+=("$TMP")
               else
                   FILES+=("$key");
               fi
               shift;;
        esac
    done

    # No options given, make our best guess.  If we're inside emacs already
    # just open it.  If we have a GUI running then popup a new frame,
    # otherwise use the current terminal.
    if [[ ! "${EMACSOPT}" ]]; then
        if [[ "$INSIDE_EMACS" ]]; then
            EMACSOPT=""
        elif [[ "$DISPLAY" ]]; then
            EMACSOPT="-c "
        else
            EMACSOPT="-nw "
        fi
    fi

    # If stdin is a pipe write it to a temporary file and open that, then
    # pass the results.  If stdout is a pipe and we and don't have a file
    # then create a temporary file.
    if [ -p /dev/stdin ]; then
        TMP="$(mktemp)"
        cat > $TMP
        FILES+=("$TMP")
        TMPFILES+=("$TMP")
    elif [[ -z "$FILES" && -p /dev/stdout ]]; then
        TMP="$(mktemp)"
        FILES+=("$TMP")
        TMPFILES+=("$TMP")
    fi

    # Open each file in the list
    for FILE in "${FILES[@]}"; do
        if [[ "$OSTYPE" == "darwin"* ]]; then
            # Use GNU readlink if available
            if hash greadlink 2>/dev/null; then
                FILE=$(greadlink -m "${FILE}")
            else
                # Hack to approximate -m flag behaviour from GNU readlink
                FILE=$(readlink "${FILE}" || echo "${FILE}")
            fi
        else
            FILE=$(readlink -m "${FILE}") # Absolute file path
        fi
        [[ $SSH_CONNECTION ]] && FILE="/ssh:${ssh_hostname}:$FILE"

        if [[ -p /dev/stdout ]];then
            emacsclient -q ${EMACSOPT} "$FILE"
            cat $FILE
        else
            function="find-file-other-window"
            case $EMACSOPT in
                *-c*) function="find-file";;
            esac
            emacsclient ${EMACSOPT} -q -e "($function \"$FILE\")"
        fi
    done

    # Remove temporary files, unless told to keep them
    if [[ "$TMPFILES" && ! "$KEEP" ]]; then
        for TMP in "${TMPFILES[@]}"; do
            # Cleanup tmp files
            rm "$TMP"
        done
    fi
}
