# e - A wrapper function for emacs and emacsclient..

## Examples:

### Open $file using emacsclient and starts emacs if there's not alreadya server running.

    e [file]
    
###  Normal emacsclient flags gets passed along.  So [c]reate a new frame for each file and [n]o-wait for the client to exit.

    e -c -n file file2

### Create a new buffer containing the output of a command

    ls | e

### Create a buffer from the output of a command, edit the results, and pass it on down the pipe.

    ls -1 *.bak | e | xargs -I{} rm {}

### Start with an empty buffer, then pass the buffer on down the pipe.

    e | rot13  # NOTE: Super Secure encryption

### Edit a file, then pass the file on down the pipe.

    e request.txt | nc www.exampke.com 80

### Edit a file as a template, then passing it to the pipe

    e -t header_template.txt | nc www.example.com 80

### Edit multiple files as templates, then passing them to the pipe

    e -t header_template.html body_template.html footer.html > new_page.html

### Keep the new templates (-k implies -t)

    e -t -k request-template.html | nc www.example.com 80

### Store the output of editing the template in ./new/ -o implies -t and -k

     e -o ./new/ template.html | nc www.example.com 80



## NOTES

Usage on remote hosts assumes you've set up the remote emacsclient to speak to the local server.

Example:

Set emacs server to tcp and listening on port 5922 then:

    function forward_emacs_ssh {
        # LC_VARs get passed with the default openssh server settings.
        # Assumes the hostname is the last argument
        LC_HOSTNAME=${*: -1} LC_EMACS=1 LC_EMACS_SERVER=$(cat ~/.emacs.d/server/server) \
                ssh -R 5922:127.0.0.1:5922  -A "$@"
    }

And on the receiving side in .bashrc or whatever:


        if [[ "$LC_EMACS_SERVER" ]]; then
            ## We're in an SSH session from inside emacs, since openssh allows $LC
            ## variables to be passed by default.  (At least on Debian 10.4)
            mkdir -p ~/.emacs.d/server 2>&1 > /dev/null
            echo "$LC_EMACS_SERVER" > ~/.emacs.d/server/server
        fi

## NOTES

By default it uses the output of the remote hosts `hostname -f` to determine the ssh_hostname.  If this is incorrect
then set ssh_hostname using another method, like above.

# NOTE:

The --template / -t option creates temporary copies of the file(s) to use.  They are deleted afterwards unless --keep / -k is
given.

